import React, { useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  Button,
} from "react-native";

import AIcon from "react-native-vector-icons/AntDesign";
import { TextInput } from "react-native-gesture-handler";

const  protracker =() =>{

  const [text1, onChangeText1] = React.useState(null);
  const [text2, onChangeText2] = React.useState(null);
  const [text3, onChangeText3] = React.useState(null);
  const [text4, onChangeText4] = React.useState(null);
  const [text5, onChangeText5] = React.useState(null);
  const [text6, onChangeText6] = React.useState(null);

  const upperchestRef = useRef();
  const middlechestRef = useRef();
  const armsRef = useRef();
  const waistRef = useRef();
  const widestwaistRef = useRef();
  const neckRef = useRef();
  
 
  return (
    <View style={styles.body}>
      <ScrollView>
        <View>
          {/* progress tracker,request */}

          <View style={styles.part1}>
            <Text style={styles.progresstrackertext}>Progress Tracker</Text>
          </View>

          <View style={styles.part1}>
            <Text style={styles.week4}>Week 4 - Day 6 (December 22)</Text>
          </View>
        </View>

        <View style={styles.box1}>
          <View style={{ flexDirection: "row", flex: 0.1 }}>
            <View style={{ flex: 2 }} />
            <View style={{ flex: 1 }}>
              <TouchableOpacity>
                <Text
                  style={{
                    color: "#E83838",
                    textDecorationLine: "underline",
                    fontWeight: "400",
                    marginTop: 10,
                  }}
                >
                  {" "}
                  Upper Body
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <TouchableOpacity>
                <Text
                  style={{ fontWeight: "400", color: "#fff", marginTop: 10 }}
                >
                  {" "}
                  Lower Body
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              flex: 0.9,
              backgroundColor: "green",
              flexDirection: "column",
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                backgroundColor: "black",
              }}
            >
              <View style={{ flex: 0.5, flexDirection: "column" }}>
                <View style={styles.neckbox}>
                  <Text style={styles.neckboxtext}>Neck</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText1}
                    value={text1}
                    placeholder="20"
                    keyboardType="numeric"
                    maxLength={3}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      upperchestRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={neckRef}
                  />

                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={{
                  flex: 0.5,
                  flexDirection: "column",
                  backgroundColor: "black",
                }}
              >
                <View style={styles.neckbox1}>
                  <Text style={styles.neckboxtext}>Upper chest</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText2}
                    value={text2}
                    placeholder="22"
                    keyboardType="numeric"
                    maxLength={3}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      middlechestRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={upperchestRef}
                   
                  />
                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                backgroundColor: "orange",
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "column",
                  backgroundColor: "black",
                }}
              >
                <View style={styles.neckbox}>
                  <Text style={styles.neckboxtext}>Middle Chest</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText3}
                    value={text3}
                    placeholder="20"
                    keyboardType="numeric"
                    maxLength={3}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      armsRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={middlechestRef}
                   
                  />
                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={{
                  flex: 0.5,
                  flexDirection: "column",
                  backgroundColor: "black",
                }}
              >
                <View style={styles.neckbox1}>
                  <Text style={styles.neckboxtext}>Arms</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText4}
                    value={text4}
                    placeholder="20"
                    keyboardType="numeric"
                    maxLength={3}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      waistRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={armsRef}
                 
                  />
                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View
              style={{ flex: 1, flexDirection: "row", backgroundColor: "aqua" }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: "column",
                  backgroundColor: "black",
                }}
              >
                <View style={styles.neckbox}>
                  <Text style={styles.neckboxtext}>Waist</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText5}
                    value={text5}
                    placeholder="20"
                    keyboardType="numeric"
                    maxLength={3}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                      widestwaistRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={waistRef}
                    
                  />
                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={{
                  flex: 0.5,
                  flexDirection: "column",
                  backgroundColor: "black",
                }}
              >
                <View style={styles.neckbox1}>
                  <Text style={styles.neckboxtext}>Widest Waist</Text>

                  <TextInput
                    style={styles.input1}
                    onChangeText={onChangeText6}
                    value={text6}
                    placeholder="20"
                    keyboardType="numeric"
                    maxLength={3} 

                    onSubmitEditing={() => {
                      neckRef.current.focus();
                    }}
                    blurOnSubmit={false}
                    ref={widestwaistRef}

                  />
                  <TouchableOpacity>
                    <AIcon
                      name="infocirlceo"
                      size={14}
                      color="#ffffff"
                      style={{
                        marginLeft: 110,
                        bottom: 20,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.lorem}>
          <Text style={styles.loremtext}>
            Lorem ipsum dolor sit consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore
          </Text>
          <TouchableOpacity>
            <AIcon
              name="infocirlceo"
              size={14}
              color="#ffffff"
              style={{
                left: 330,
              }}
            />
          </TouchableOpacity>
        </View>

        <View style={{ marginTop: 50 }}>
          <View style={{ flex: 1, flexDirection: "column" }}>
            <View style={{ flex: 0.5 }}>
              <Image
                source={require("../../assets1/image12.png")}
                style={styles.image12}
              />
            </View>
            <Image
              source={require("../../assets1/image13.png")}
              style={styles.image13}
            />
            <Image
              source={require("../../assets1/image14.png")}
              style={styles.image14}
            />
            <Image
              source={require("../../assets1/image15.png")}
              style={styles.image15}
            />
          </View>
        </View>

        <View>
          <View style={styles.frontimagebox}>
            <Image
              source={require("../../assets1/vector4.png")}
              style={styles.vector4}
            />
          </View>

          <View style={styles.sideimagebox}>
            <Image
              source={require("../../assets1/vector4.png")}
              style={styles.vector4}
            />
          </View>

          <View style={styles.backimagebox}>
            <Image
              source={require("../../assets1/vector4.png")}
              style={styles.vector4}
            />
          </View>

          <View style={styles.angularimagebox}>
            <Image
              source={require("../../assets1/vector4.png")}
              style={styles.vector4}
            />
          </View>

          <Text style={styles.fronttext}>Front</Text>

          <Text style={styles.sidetext}>Side</Text>

          <Text style={styles.backtext}>Back</Text>

          <Text style={styles.angulartext}>Angular</Text>
          <View></View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: "black",
  },

  headerContainer: {
    backgroundColor: "#000",
    width: "100%",
    height: 70,
  },

  part1: {
    flex: 1,
    backgroundColor: "#000",
    width: "100%",
    height: 50,
  },

  progresstrackertext: {
    fontSize: 18,
    textAlign: "left",
    top: 10,
    left: 40,
    color: "#fff",
    fontWeight: "700",
  },

  week4: {
    fontSize: 18,
    textAlign: "left",
    left: 40,
    color: "#fff",
    fontWeight: "700",
  },

  box1: {
    backgroundColor: "black",
    flexDirection: "column",
    flex: 1,
    height: 370,
    borderWidth: 1,
    borderColor: "#272727",
    borderRadius: 10,
    width: "90%",
    alignSelf: "center",
  },

  upperbodyview: {
    backgroundColor: "black",
    height: 40,
    width: 100,
    left: 170,
  },

  upperbodytext: {
    textAlign: "right",
    color: "#fff",
    marginLeft: 80,
    top: 10,
    textDecorationLine: "underline",
    fontWeight: "400",
  },

  lowerbodyview: {
    backgroundColor: "black",
    height: 40,
    width: 120,
    left: 200,
  },

  lowerbodytext: {
    textAlign: "right",
    bottom: 8,
    fontWeight: "400",
    color: "#fff",
  },

  box2: {
    backgroundColor: "#000",
    height: 10,
    width: "90%",
    left: 20,
  },

  neckbox: {
    backgroundColor: "black",
    height: 80,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 10,
    width: 140,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
    marginLeft: 20,
  },

  neckbox1: {
    backgroundColor: "black",
    height: 80,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 10,
    width: 140,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
    marginRight: 20,
  },

  neckboxtext: {
    fontSize: 14,
    textAlign: "center",
    color: "#fff",
    fontWeight: "500",
    top: 3,
  },

  upperchestbox: {
    backgroundColor: "black",
    height: 80,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 10,
    width: 140,
    justifyContent: "center",
    alignItems: "center",
    bottom: 280,
    marginLeft: 170,
  },

  armsbox: {
    backgroundColor: "black",
    height: 80,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 10,
    width: 140,
    justifyContent: "center",
    alignItems: "center",
    bottom: 260,
    marginLeft: 170,
  },

  widestbox: {
    backgroundColor: "black",
    height: 80,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 10,
    width: 140,
    justifyContent: "center",
    alignItems: "center",
    bottom: 240,
    marginLeft: 170,
  },

  lorem: {
    flex: 1,
    backgroundColor: "#272727",
    top: 45,
    height: 70,
    borderWidth: 0.9,
    borderColor: "#272727",
    borderRadius: 6,
    width: "90%",
    alignSelf: "center",
  },

  loremtext: {
    fontSize: 15,
    textAlign: "left",
    top: 5,
    left: 10,
    color: "#fff",
    fontWeight: "400",
  },

  frontimagebox: {
    backgroundColor: "black",
    height: 93,
    borderWidth: 0.9,
    borderColor: "#8f5f24",
    borderRadius: 10,
    width: 70,
    marginLeft: 40,
    bottom: 170,
  },

  sideimagebox: {
    backgroundColor: "black",
    height: 93,
    borderWidth: 0.9,
    borderColor: "#8f5f24",
    borderRadius: 10,
    width: 70,
    marginLeft: 125,
    bottom: 263,
  },

  backimagebox: {
    backgroundColor: "black",
    height: 93,
    borderWidth: 0.9,
    borderColor: "#8f5f24",
    borderRadius: 10,
    width: 70,
    marginLeft: 205,
    bottom: 357,
  },

  angularimagebox: {
    backgroundColor: "black",
    height: 93,
    borderWidth: 0.9,
    borderColor: "#8f5f24",
    borderRadius: 10,
    width: 70,
    marginLeft: 285,
    bottom: 450,
  },

  fronttext: {
    color: "#fff",
    flex: 1,
    flexDirection: "row",
    fontSize: 15,
    width: 50,
    bottom: 440,
    left: 60,
  },

  sidetext: {
    color: "#fff",
    flex: 1,
    flexDirection: "row",
    fontSize: 15,
    width: 50,
    bottom: 460,
    left: 150,
  },

  backtext: {
    color: "#fff",
    flex: 1,
    flexDirection: "row",
    fontSize: 15,
    width: 50,
    bottom: 480,
    left: 230,
  },

  angulartext: {
    color: "#fff",
    flex: 1,
    flexDirection: "row",
    fontSize: 15,
    width: 70,
    bottom: 500,
    left: 300,
  },

  input1: {
    height: 30,
    width: 80,
    padding: 5,
    color: "#fff",
    backgroundColor: "#272727",
    borderWidth: 0.1,
    fontSize: 14,
    textAlign: "center",
    marginTop: 10,
  },

  image12: {
    height: 70,
    width: 70,
    marginStart: 20,
    marginTop: 30,
    marginLeft: 40,
  },

  image13: {
    height: 70,
    width: 70,
    marginLeft: 120,
    bottom: 70,
  },

  image14: {
    height: 70,
    width: 70,
    marginLeft: 200,
    bottom: 140,
  },

  image15: {
    height: 70,
    width: 70,
    marginLeft: 280,
    bottom: 210,
  },

  vector4: {
    height: 35,
    width: 40,
    marginLeft: 15,
    marginTop: 30,
  },
});

export default protracker;
